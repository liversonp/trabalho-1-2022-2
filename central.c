#include <stdio.h> 
#include <string.h>   //strlen 
#include <stdlib.h> 
#include <errno.h> 
#include <unistd.h>   //close 
#include <arpa/inet.h>    //close 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <pthread.h>
#include <semaphore.h>
#include <sys/time.h> //FD_SET, FD_ISSET, FD_ZERO macros 
#include "cJSON.h"

#define TRUE   1 
#define FALSE  0 
#define PORT 10311

int choice = -1, room = 0, new_socket, qtdcli=0, client_socket[30];
sem_t sem_choice, sem_room, sem_startMenu;

struct clientvalues{
    int lamp1,lamp2,arcond,spres,spor,sjan,sfum, proj, albz;
} ClientValues[30];

void mandarmsg(char *buffer, int *client_socket, int max_clients){
    int sd, i;

    if(room != 0){
        if((sd = client_socket[room-1])!=0){
            send(sd,buffer,strlen(buffer),0);
        }

        else
            printf("Sala nao esta conectada\n");

        
        strcpy(buffer,"ok");
        for(i = 0;i <max_clients; i++){
            sd = client_socket[i];
            if(client_socket[room-1] != sd){
                send(sd,buffer,strlen(buffer),0);
            }
        }

    }

    else{
        for(i = 0;i <max_clients; i++){
            sd = client_socket[i];
            send(sd,buffer,strlen(buffer),0);
        }
    }
    choice = -1;
}
     
void communication(int master_socket, int max_clients, int *client_socket, struct sockaddr_in address){
    char *message = "ECHO Daemon v1.0 \r\n";
    char buffer[1025];  //data buffer of 1K 
    int i, max_sd, valread, sd, activity, addrlen, j;
    //set of socket descriptors 
    fd_set readfds;
    addrlen = sizeof(address); 
    
    while(TRUE){
        //clear the socket set 
        FD_ZERO(&readfds);  
        max_sd = master_socket;
     
        //add master socket to set 
        FD_SET(master_socket, &readfds);  
             
        //add child sockets to set 
        for ( i = 0 ; i < max_clients ; i++){  
            //socket descriptor 
            sd = client_socket[i];  
                 
            //if valid socket descriptor then add to read list 
            if(sd > 0)  
                FD_SET( sd , &readfds);  
                 
            //highest file descriptor number, need it for the select function 
            if(sd > max_sd)  
                max_sd = sd;  
        }  
     
        //wait for an activity on one of the sockets , timeout is NULL , 
        //so wait indefinitely 
        activity = select( max_sd + 1 , &readfds , NULL , NULL , NULL);  
       
        if ((activity < 0) && (errno!=EINTR)){  
            printf("select error");  
        }  
             
        //If something happened on the master socket , 
        //then its an incoming connection 
        if (FD_ISSET(master_socket, &readfds)){
            sem_wait(&sem_startMenu);
            if ((new_socket = accept(master_socket,(struct sockaddr *)&address, (socklen_t*)&addrlen))<0){  
                perror("accept");  
                exit(EXIT_FAILURE);  
            }  
             
            //inform user of socket number - used in send and receive commands 
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));  
            qtdcli++;
            //send new connection greeting message 
            if( send(new_socket, message, strlen(message), 0) != strlen(message)){  
                perror("send");  
            }  
                
            puts("Welcome message sent successfully");  
                 
            //add new socket to array of sockets 
            for (i = 0; i < max_clients; i++){
                //if position is empty 

                if( client_socket[i] == 0 ){  
                    client_socket[i] = new_socket;  
                    printf("Adding to list of sockets as %d\n" , i);  

                    break;  
                }  
            }
            sem_post(&sem_startMenu);
        }
             
        //else its some IO operation on some other socket
        for (i = 0; i < max_clients; i++){  
            sd = client_socket[i];  
                 
            if (FD_ISSET( sd , &readfds)){  
                //Check if it was for closing , and also read the 
                //incoming message 
                if ((valread = read( sd , buffer, 1024)) == 0){  
                    //Somebody disconnected , get his details and print 
                    getpeername(sd, (struct sockaddr*)&address, (socklen_t*)&addrlen);  
                    
                    printf("Host disconnected , ip %s , port %d \n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));     
                    qtdcli--;
                    //Close the socket and mark as 0 in list for reuse 
                    close( sd );  
                    client_socket[i] = 0;
                }  
                     
                //Echo back the message that came in 
                else{  
                    //set the string terminating NULL byte on the end 
                    //of the data read 
                    buffer[valread] = '\0';
                    ClientValues[i].lamp1 = (int)buffer[0] - 48;
                    ClientValues[i].lamp2 = (int)buffer[1] - 48;
                    ClientValues[i].arcond = (int)buffer[2] - 48;
                    ClientValues[i].spres = (int)buffer[3] - 48;
                    ClientValues[i].spor = (int)buffer[4] - 48;
                    ClientValues[i].sjan = (int)buffer[5] - 48;
                    ClientValues[i].sfum = (int)buffer[6] - 48;
                    ClientValues[i].proj = (int)buffer[7] - 48;
                    ClientValues[i].albz = (int)buffer[8] - 48;
                }
            }
            sem_wait(&sem_room);
            if(room != -1){
                sem_wait(&sem_choice);
                if(choice == 1){
                    strcpy(buffer,"val1");
                    mandarmsg(buffer,client_socket,max_clients);
                }
                else if(choice == 2){
                    strcpy(buffer,"val2");
                    mandarmsg(buffer,client_socket,max_clients);
                }
                else if(choice == 3){
                    strcpy(buffer,"val3");
                    mandarmsg(buffer,client_socket,max_clients);
                }
                else if(choice == 4){
                    strcpy(buffer,"val4");
                    mandarmsg(buffer,client_socket,max_clients);
                }
                else if(choice == 5){
                    strcpy(buffer,"val5");
                    mandarmsg(buffer,client_socket,max_clients);
                }
                else if(choice == 6){
                    strcpy(buffer,"val6");
                    mandarmsg(buffer,client_socket,max_clients);
                }

                else{
                    strcpy(buffer,"ok");
                    for(i = 0;i <max_clients; i++){
                        sd = client_socket[i];
                        send(sd,buffer, strlen(buffer), 0);
                    }
                }
                sem_post(&sem_choice);
                sem_post(&sem_room);

            }
            else{
                sem_wait(&sem_choice);
                printf("encerrando tudo\n");
                strcpy(buffer,"exit");
                
                for(i = 0;i <max_clients; i++){
                    sd = client_socket[i];
                    send(sd,buffer, strlen(buffer), 0);
                }
                choice = -1;
                sem_post(&sem_choice);
                sem_post(&sem_room);
                return;
            }
        }
    }
}

void *menu(){
    int startmenu = 0, i, aux;
    while(startmenu == 0){
        sem_wait(&sem_startMenu);
        if(new_socket != 0){
            startmenu = new_socket;
        }
        sem_post(&sem_startMenu);
    }

    while(1){

        printf("Este eh o menu de controle\n");
        printf("____________________________________________\n");
        printf("Caso deseje encerrar o programa digite -1\n");
        printf("Digite o numero da sala que deseja controlar\n");
        printf("Digite 0 para controlar todas as salas\n");
        printf("____________________________________________\n");
        sem_wait(&sem_room);
        scanf("%d", &room);
        sem_post(&sem_room);
        
        if(room == -1)
            break;
        printf("____________________________________________\n");
        printf("____________________________________________\n");
        printf("Digite o número da opcao que desejar\n");
        printf("1 - Ligar/Desligar Lampada 1\n");
        printf("2 - Ligar/Desligar Lampada 2\n");
        printf("3 - Ligar/Desligar todas as Lampadas\n");
        printf("4 - Ligar/Desligar Projetor\n");
        printf("5 - Ligar/Desligar Sensor de alarme\n");
        printf("6 - Ligar/Desligar Ar-condicionado\n");
        printf("7 - Mostrar informações das salas\n");
        printf("____________________________________________\n");
        sem_wait(&sem_choice);
        scanf("%d", &choice);
        sem_post(&sem_choice);
        sem_wait(&sem_choice);
        if(choice == 7){
            if(room == 0){
                for(i = 0; i <qtdcli;i++){
                    printf("Sala %d: lampada1 %d\n", i+1, ClientValues[i].lamp1);
                    printf("Sala %d: lampada2 %d\n", i+1, ClientValues[i].lamp2);
                    printf("Sala %d: ar-condicionado %d\n", i+1, ClientValues[i].arcond);
                    printf("Sala %d: Projetor %d\n", i+1, ClientValues[i].proj);
                    printf("Sala %d: Sensor de presenca %d\n", i+1, ClientValues[i].spres);
                    printf("Sala %d: sensor porta %d\n", i+1, ClientValues[i].spor);
                    printf("Sala %d: sensor janela %d\n", i+1, ClientValues[i].sjan);
                    printf("Sala %d: Sensor de Fumaca %d\n", i+1, ClientValues[i].sfum);
                    printf("Sala %d: Alarme Buzzer %d\n", i+1, ClientValues[i].albz);
                }
            }
            else{
                printf("Sala %d: lampada1 %d\n", room, ClientValues[room-1].lamp1);
                printf("Sala %d: lampada2 %d\n", room, ClientValues[room-1].lamp2);
                printf("Sala %d: ar-condicionado %d\n", room, ClientValues[room-1].arcond);
                printf("Sala %d: Projetor %d\n", room, ClientValues[room-1].proj);
                printf("Sala %d: presenca %d\n", room, ClientValues[room-1].spres);
                printf("Sala %d: porta %d\n", room, ClientValues[room-1].spor);
                printf("Sala %d: janela %d\n", room, ClientValues[room-1].sjan);
                printf("Sala %d: Sensor de fumaca %d\n", room, ClientValues[room-1].sfum);
                printf("Sala %d: Alarme-Buzzer %d\n", room, ClientValues[room-1].albz);
            }
        }
        sem_post(&sem_choice);
        usleep(100);
    }
}

int main(int argc , char *argv[]){  
    int opt = TRUE;  
    int master_socket,max_clients = 30, valread, i;  
    struct sockaddr_in address;
    pthread_t t_menu,t_rcvinfos;
    FILE *json;
    cJSON *ip_servidor_central = NULL;
    cJSON *file_parser = NULL;
    char filebff[5200];
    json = fopen("configuracao_sala_01.json", "r");

    fread(filebff,5048,1,json);
    file_parser = cJSON_Parse(filebff);

    if(file_parser == NULL){
        printf("Parse falhou...\n");
        exit(-1);
    }

    ip_servidor_central = cJSON_GetObjectItem(file_parser,"ip_servidor_central");
    printf("%s\n", ip_servidor_central->valuestring);
    fclose(json);
    //initialise all client_socket[] to 0 so not checked 
    for (i = 0; i < max_clients; i++){  
        client_socket[i] = 0;  
    }  
         
    //create a master socket 
    if((master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0){  
        perror("socket failed");  
        exit(EXIT_FAILURE);  
    }  
     
    //set master socket to allow multiple connections , 
    //this is just a good habit, it will work without this 
    if( setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0 ){  
        perror("setsockopt");  
        exit(EXIT_FAILURE);  
    }  
     
    //type of socket created 
    address.sin_family = AF_INET;  
    address.sin_addr.s_addr = inet_addr(ip_servidor_central->valuestring);
    address.sin_port = htons( PORT );  
         
    //bind the socket to localhost port 8888 
    if (bind(master_socket, (struct sockaddr *)&address, sizeof(address))<0){  
        perror("bind failed");  
        exit(EXIT_FAILURE);  
    }

    printf("Listener on port %d \n", PORT);  
         
    //try to specify maximum of 3 pending connections for the master socket 
    if (listen(master_socket, 3) < 0){  
        perror("listen");  
        exit(EXIT_FAILURE);  
    }  
         
    //accept the incoming connection  
    puts("Waiting for connections ...");

    sem_init(&sem_choice,0,1);
    sem_init(&sem_room,0,1);
    sem_init(&sem_startMenu,0,1);
    pthread_create(&t_menu, NULL, menu, NULL);

    communication(master_socket, max_clients, client_socket, address);
    sem_close(&sem_choice);
    sem_close(&sem_room);
    sem_close(&sem_startMenu);
    pthread_join(t_menu, NULL);
    return 0;
}