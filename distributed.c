// Client side C/C++ program to demonstrate Socket
// programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include "cJSON.h"
#include <bcm2835.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>

#define MAXTIMINGS 100

int presenca = 0, alarme=0;
int L_01, L_02, AC, PR, AL_BZ;
int SPres, SFum, SJan, SPor, SC_IN, SC_OUT, DHT22;
int isrunning = 1;

int SPresout = 0,SFumout = 0,SJanout = 0, SPorout = 0, L_01out = 0, L_02out = 0, AL_BZout = 0;
int PRout = 0, ACout = 0, qtdPessoas;
char sensores[9];
float mytemp, myhum, TEMPout, HUMout,sock = 0;

sem_t sem_lamp1, sem_lamp2, sem_alarm, sem_run;

void configurePins(int pin,char alltags[][38],char tag[]){
    int value, i;
    for(i=0;i<12;i++){
        if(strcmp(alltags[i],tag) == 0){
            value = i;
            break;
        }
    }
    switch(value){
        case 0:
            L_01 = pin;
            break;
        case 1:
            L_02 = pin;
            break;
        case 2:
            PR = pin;
            break;
        case 3:
            AC = pin;
            break;
        case 4:
            AL_BZ = pin;
            break;
        case 5:
            SPres = pin;
            break;
        case 6:
            SFum = pin;
            break;
        case 7:
            SJan = pin;
            break;
        case 8:
            SPor = pin;
            break;
        case 9:
            SC_IN = pin;
            break;
        case 10:
            SC_OUT = pin;
            break;
    }
}

void setPins(){

    // Define botão como entrada
    bcm2835_gpio_fsel(SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_OUT, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-down
    bcm2835_gpio_set_pud(SPres, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SFum, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SJan, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SPor, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_IN, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_OUT, BCM2835_GPIO_PUD_DOWN);

    // Configura pinos dos LEDs como saídas
    bcm2835_gpio_fsel(L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AL_BZ, BCM2835_GPIO_FSEL_OUTP);
}

void getSensors(){
    sensores[0] = L_01out+'0';
    sensores[1] = L_02out+'0';
    sensores[2] = ACout+'0';
    sensores[3] = SPresout+'0';
    sensores[4] = SPorout+'0';
    sensores[5] = SJanout+'0';
    sensores[6] = SFumout+'0';
    sensores[7] = PRout+'0';
    sensores[8] = AL_BZout+'0';
}

void desligarTudo(){
    bcm2835_gpio_write(L_01, LOW);
    bcm2835_gpio_write(L_02, LOW);
    bcm2835_gpio_write(AC, LOW);
    bcm2835_gpio_write(PR, LOW);
    bcm2835_gpio_write(AL_BZ, LOW);
}

void ligarDesligarLamp1(int command){
    if(command == 1){
        bcm2835_gpio_write(L_01, HIGH);
        L_01out = 1;
    }
    else{
        bcm2835_gpio_write(L_01, LOW);
        L_01out = 0;
    }
}

void ligarDesligarLamp2(int command){
    if(command == 1){
        bcm2835_gpio_write(L_02, HIGH);
        L_02out = 1;
    }
    else{
        bcm2835_gpio_write(L_02, LOW);
        L_02out = 0;
    }
}

void ligarDesligarProjetor(int command){
    if(command == 1){
        bcm2835_gpio_write(PR, HIGH);
        PRout = 1;
    }
    else{
        bcm2835_gpio_write(PR, LOW);
        PRout = 0;
    }
}

void ligarDesligarAlarme(int command){
    if(command == 1){
        alarme = 1;
    }
    else{
        alarme = 0;
        AL_BZout =0;
        bcm2835_gpio_write(AL_BZ, LOW);
    }
}

void ligarDesligarAr(int command){
    if(command == 1){
        printf("Ligando ar-condicionado...\n");
        bcm2835_gpio_write(AC, HIGH);
        ACout = 1;
    }
    else{
        printf("Desligando ar-condicionado...\n");
        bcm2835_gpio_write(AC, LOW);
        ACout = 0;
    }
}


int readDHT(int pin, float *humid0, float *temp0) {
	int counter = 0;
	int laststate = HIGH;
	int j=0;

	int bits[250], data[100];
	int bitidx = 0;

  	// Set GPIO pin to output
  	bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);

  	bcm2835_gpio_write(pin, HIGH);
  	usleep(500000);  // 500 ms
 	bcm2835_gpio_write(pin, LOW);
  	usleep(20000);

  	bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);

  	data[0] = data[1] = data[2] = data[3] = data[4] = 0;

  	// wait for pin to drop?
  	while (bcm2835_gpio_lev(pin) == 1) {
    		usleep(1);
  	}

  	// read data!
  	for (int i=0; i< MAXTIMINGS; i++) {
	    	counter = 0;
    		while ( bcm2835_gpio_lev(pin) == laststate) {
			counter++;
			//nanosleep(1);		// overclocking might change this?
	        	if (counter == 1000)
		  		break;
    		}
    		
		laststate = bcm2835_gpio_lev(pin);
    		if (counter == 1000) break;
   		bits[bitidx++] = counter;

    		if ((i>3) && (i%2 == 0)) {
      			// shove each bit into the storage bytes
      			data[j/8] <<= 1;
      			if (counter > 200)
        			data[j/8] |= 1;
      			j++;
    		}
	}


#ifdef DEBUG
	for (int i=3; i<bitidx; i+=2) {
		printf("bit %d: %d\n", i-3, bits[i]);
    		printf("bit %d: %d (%d)\n", i-2, bits[i+1], bits[i+1] > 200);
  	}
  	printf("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x\n", j, data[0], data[1], data[2], data[3], data[4]);
#endif

  	if ((j >= 39) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) ) {	// yay!
		float f, h;
		h = data[0] * 256 + data[1];
		h /= 10;

		f = (data[2] & 0x7F)* 256 + data[3];
        	f /= 10.0;
        	if (data[2] & 0x80) {
			f *= -1;
		}
		printf("Temp =  %.1f *C, Hum = %.1f \%\n", f, h);
	
		*humid0 = h;
		*temp0 = f;
	}	
  	return 0;
}

void *controlaLampadas(){
    while(1){
        if(isrunning == 0){
            break;
        }

        if(bcm2835_gpio_lev(SPres)){
            if(alarme == 0){
                bcm2835_gpio_write(AL_BZ, LOW);
                AL_BZout =0;
                sem_wait(&sem_lamp1);
                ligarDesligarLamp1(1);
                sem_post(&sem_lamp1);
                sem_wait(&sem_lamp2);
                ligarDesligarLamp2(1);
                sem_post(&sem_lamp2);
            }
            else{
                bcm2835_gpio_write(AL_BZ, HIGH);
                AL_BZout =1;
            }
        }
        else{
            if(L_01out == 1 && L_02out == 1){
                delay(15000);
                sem_wait(&sem_lamp1);
                ligarDesligarLamp1(0);
                sem_post(&sem_lamp1);
                sem_wait(&sem_lamp2);
                ligarDesligarLamp2(0);
                sem_post(&sem_lamp2);
            }
        }
    }
}

void *medeTemperatura(){
    while(1){
    
    if(isrunning == 0){
        break;
    }
    
    delay(50);
    readDHT(DHT22, &mytemp, &myhum);
    if(mytemp != 0 && myhum != 0){
        TEMPout = mytemp;
        HUMout = myhum;
    }
    //printf("temperatura %.1f *C, Humidade %.1f \%\n", mytemp, myhum);
    }
}

void *controlaSensores(){
    int pessoaIN = 0,pessoaOUT = 0, tempTimer = 0,entrando = 0,saindo = 0,lampadaTimer=0;
    while(1){
        if(isrunning == 0){
            break;
        }
        
        lampadaTimer++;
        tempTimer++;
        if(bcm2835_gpio_lev(SC_IN)){
            pessoaIN++;
        }
        else if(bcm2835_gpio_lev(SC_OUT)){
            pessoaOUT++;
        }
        else{
            if(pessoaIN >= 2 && pessoaIN <= 6){
                entrando = 1;
            }

            if(pessoaOUT >= 2 && pessoaOUT <= 6){
                saindo = 1;
            }
            qtdPessoas += entrando;
            qtdPessoas -= saindo;
            pessoaIN = 0;
            pessoaOUT = 0;
            entrando = 0;
            saindo = 0;
        }
        if(bcm2835_gpio_lev(SFum)){
            ligarDesligarAlarme(1);
        }
        else{
            if(SFumout == 0)
                ligarDesligarAlarme(0);
        }
        if(tempTimer >= 40){
            printf("Quantidade de pessoas: %d\n", qtdPessoas);
            tempTimer = 0;
        }
        delay(50);
    }
}

 
int main(int argc, char const* argv[]){
    if(argc < 2){
        printf("Precisa passar o arquivo a ser lido ao programa!\n");
        exit(0);
    }


    int i;
    char archive[1000];
    FILE *json;
    cJSON *ip_servidor_central = NULL;
    cJSON *porta_servidor_central = NULL;
    cJSON *ip_servidor_distribuido = NULL;
    cJSON *porta_servidor_distribuido = NULL;
    cJSON *file_parser = NULL;
    cJSON *nome = NULL;
    cJSON* dispositivo = NULL;
    cJSON *type = NULL;
    cJSON *tag = NULL;
    cJSON *pino = NULL;
    cJSON *outputs = NULL;
    cJSON *inputs = NULL;
    cJSON *sensortemp = NULL;
    char filebff[5200];
    char alltags[13][38] = {"Lâmpada 01", "Lâmpada 02", "Projetor Multimidia", "Ar-Condicionado (1º Andar)", "Sirene do Alarme", "Sensor de Presença", "Sensor de Fumaça", "Sensor de Janela", "Sensor de Porta", "Sensor de Contagem de Pessoas Entrada", "Sensor de Contagem de Pessoas Saída"};

    strcpy(archive,argv[1]);
    json = fopen(archive, "r");

    fread(filebff,5048,1,json);
    file_parser = cJSON_Parse(filebff);

    if(file_parser == NULL){
        printf("Parse falhou...\n");
        exit(-1);
    }

    ip_servidor_central = cJSON_GetObjectItem(file_parser,"ip_servidor_central");
    porta_servidor_central = cJSON_GetObjectItem(file_parser, "porta_servidor_central");
    ip_servidor_distribuido = cJSON_GetObjectItem(file_parser,"ip_servidor_distribuido");
    porta_servidor_distribuido = cJSON_GetObjectItem(file_parser, "porta_servidor_distribuido");
    nome = cJSON_GetObjectItem(file_parser, "nome");
    outputs = cJSON_GetObjectItem(file_parser, "outputs");
    for(i = 0; i < cJSON_GetArraySize(outputs);i++){
        dispositivo = cJSON_GetArrayItem(outputs, i);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        configurePins(pino->valueint, alltags, tag->valuestring);
    }

    inputs = cJSON_GetObjectItem(file_parser, "inputs");
    for(i = 0; i < cJSON_GetArraySize(inputs);i++){
        dispositivo = cJSON_GetArrayItem(inputs, i);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        configurePins(pino->valueint, alltags, tag->valuestring);
    }

    sensortemp = cJSON_GetObjectItem(file_parser, "sensor_temperatura");

        dispositivo = cJSON_GetArrayItem(sensortemp, 0);
        type = cJSON_GetObjectItem(dispositivo, "type");
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        pino = cJSON_GetObjectItem(dispositivo, "gpio");
        DHT22 = pino->valueint;

    fclose(json);

    if(!bcm2835_init())
        exit(1);

    setPins();

    int valread, client_fd;
    char ip[17];
    int PORT = 10311;
    struct sockaddr_in serv_addr;
    char buffer[1024] = { 0 };

    pthread_t t_lamp, t_dht22, t_sensores, t_sndinfos;

    for(i=0; i<4;i++){
        sensores[i] = '0';
    }

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        perror("Erro na criacao do socket");
        return -1;
    }

 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
 
    // Convert IPv4 and IPv6 addresses from text to binary
    if (inet_pton(AF_INET, ip_servidor_central->valuestring, &serv_addr.sin_addr)
        <= 0) {
        perror("\n Erro com o endereco \n");
        return -1;
    }

    client_fd = connect(sock, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

    if (client_fd < 0){
        perror("Conexao falhou");
        return -1;
    }

    printf("Conectado ao servidor central!\n");
    //sem_running, sem_lamp1, sem_lamp2, sem_fum, sem_jan, sem_porta, sem_pres;
    sem_init(&sem_lamp1, 0,1);
    sem_init(&sem_lamp2, 0,1);

    pthread_create(&t_lamp,NULL,controlaLampadas,NULL);
    pthread_create(&t_dht22, NULL, medeTemperatura, NULL);
    pthread_create(&t_sensores, NULL, controlaSensores, NULL);

    while(1){
        bzero(buffer, 1024);
        valread = read(sock, buffer, 1024);
        
        if(strncmp(buffer, "exit", 5) == 0){
            isrunning = 0;
            desligarTudo();
            break;
        }

        else{
            getSensors();
            if(strncmp(buffer,"val1", 4) == 0){
                sem_wait(&sem_lamp1);
                ligarDesligarLamp1(!L_01out);
                sem_post(&sem_lamp1);
            }

            if(strncmp(buffer,"val2", 4) == 0){
                sem_wait(&sem_lamp2);
                ligarDesligarLamp2(!L_02out);
                sem_post(&sem_lamp2);
            }

            if(strncmp(buffer,"val3", 4) == 0){
                sem_wait(&sem_lamp1);
                ligarDesligarLamp1(!L_01out);
                sem_post(&sem_lamp1);
                sem_wait(&sem_lamp2);
                ligarDesligarLamp2(!L_02out);
                sem_post(&sem_lamp2);
            }

            if(strncmp(buffer,"val4", 4) == 0){
                ligarDesligarProjetor(!PRout);
            }

            if(strncmp(buffer,"val5", 4) == 0){
                ligarDesligarAlarme(!SFumout);
                SFumout = !SFumout;
            }

            if(strncmp(buffer,"val6", 4) == 0){
                ligarDesligarAr(!ACout);
            }
            strncpy(buffer,sensores,9);
            send(sock, buffer, strlen(buffer), 0); 
        }
    }

    // closing the connected socket
    close(client_fd);

    pthread_join(t_lamp,NULL);
    pthread_join(t_dht22,NULL);
    pthread_join(t_sensores,NULL);
    sem_close(&sem_lamp1);
    sem_close(&sem_lamp2);
    printf("fechando o programa!\n");
    return 0;
}